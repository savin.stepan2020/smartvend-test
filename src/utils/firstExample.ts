export default [
  {
    date: new Date(2022, 9, 15),
    value: 4912324,
  },
  {
    date: new Date(2022, 9, 16),
    value: 5023654,
  },
  {
    date: new Date(2022, 9, 17),
    value: 5630021,
  },
  {
    date: new Date(2022, 9, 18),
    value: 5324032,
  },
  {
    date: new Date(2022, 9, 19),
    value: 5109213,
  },
  {
    date: new Date(2022, 9, 20),
    value: 4910322,
  },
];
