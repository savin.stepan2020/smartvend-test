type dataPoint = {
  value: number;
  date: Date;
};

type dataPoints = dataPoint[];

export { dataPoint, dataPoints };
