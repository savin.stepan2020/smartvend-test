export default [
    {
      date: new Date(2022, 9, 15),
      value: 13020,
    },
    {
      date: new Date(2022, 9, 16),
      value: 32021,
    },
    {
      date: new Date(2022, 9, 17),
      value: 42300,
    },
    {
      date: new Date(2022, 9, 18),
      value: 22128,
    },
    {
      date: new Date(2022, 9, 19),
      value: 15003,
    },
    {
      date: new Date(2022, 9, 20),
      value: 38920,
    },
  ];
  