export default [
    {
      date: new Date(2022, 9, 15),
      value: 1230,
    },
    {
      date: new Date(2022, 9, 16),
      value: 4321,
    },
    {
      date: new Date(2022, 9, 17),
      value: 3211,
    },
    {
      date: new Date(2022, 9, 18),
      value: 998,
    },
    {
      date: new Date(2022, 9, 19),
      value: 3123,
    },
    {
      date: new Date(2022, 9, 20),
      value: 5680,
    },
  ];
  