import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import { dataPoints } from "../../typings/typings";

const initChart = (id: string, incomingData: dataPoints, titleText: string) => {
  am4core.useTheme(am4themes_animated);
  const chart = am4core.create(id, am4charts.XYChart);

  chart.paddingTop = 32;
  chart.paddingLeft = 32;
  chart.paddingRight = 32;
  chart.paddingLeft = 32;
  chart.background.fill = am4core.color("#212122");
  chart.layout = "vertical";

  chart.numberFormatter.numberFormat = "#,###";

  const title = chart.titles.create();
  title.text = titleText;
  title.align = "left";
  title.fontWeight = "500";
  title.fontSize = "16";
  title.fill = am4core.color("#FFF");
  title.marginBottom = 48;

  const dateAxis = chart.xAxes.push(new am4charts.DateAxis());
  dateAxis.renderer.labels.template.fill = am4core.color("#FFF");
  dateAxis.renderer.labels.template.fontSize = 10;
  dateAxis.renderer.labels.template.fontWeight = "700";
  dateAxis.renderer.labels.template.marginTop = 24;
  dateAxis.renderer.grid.template.strokeOpacity = 0;
  dateAxis.startLocation = 1.4;
  dateAxis.endLocation = -0.4;
  dateAxis.renderer.minGridDistance = 30;
  dateAxis.tooltipDateFormat = "dd.MM.yyyy [opacity: 50%]HH:mm:ss[/]";

  const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
  valueAxis.renderer.minGridDistance = 50;
  valueAxis.renderer.labels.template.fill = am4core.color("#FFF");
  valueAxis.renderer.labels.template.fontSize = 10;
  valueAxis.renderer.labels.template.fontWeight = "700";
  valueAxis.renderer.grid.template.stroke = am4core.color("#FFF");
  valueAxis.renderer.grid.template.strokeOpacity = 0.05;
  valueAxis.strictMinMax = true;
  valueAxis.cursorTooltipEnabled = false;

  const gradient = new am4core.LinearGradient();
  gradient.addColor(am4core.color("rgba(33, 33, 34, 0.2)"));
  gradient.addColor(am4core.color("rgba(0, 163, 255, 0.2)"));
  gradient.rotation = -90;

  const series = chart.series.push(new am4charts.LineSeries());
  series.dataFields.valueY = "value";
  series.dataFields.dateX = "date";
  series.stroke = am4core.color("#00A3FF");
  series.strokeWidth = 2;
  series.tensionX = 0.7;
  series.fill = gradient;
  series.fillOpacity = 1;

  const bullet = series.bullets.push(new am4core.Circle());
  bullet.radius = 4;
  bullet.fill = am4core.color("#FFF");
  bullet.strokeWidth = 2;
  bullet.scale = 0;
  const hoverState = bullet.states.create("hover");
  hoverState.properties.scale = 1;

  const seriesTooltip = new am4core.Tooltip();
  series.tooltip = seriesTooltip;
  seriesTooltip.background.pointerLength = 6;
  seriesTooltip.background.pointerBaseWidth = 10;
  seriesTooltip.getFillFromObject = false;
  seriesTooltip.background.fill = am4core.color("#131313");
  seriesTooltip.background.fillOpacity = 1;
  seriesTooltip.background.strokeWidth = 0;
  seriesTooltip.background.cornerRadius = 8;
  seriesTooltip.label.fontSize = 12;
  seriesTooltip.label.paddingTop = 12;
  seriesTooltip.label.paddingBottom = 12;
  seriesTooltip.label.paddingLeft = 16;
  seriesTooltip.label.paddingRight = 16;

  seriesTooltip.dy = -6;
  seriesTooltip.pointerOrientation = "vertical";
  series.tooltipText = "{valueY}";

  seriesTooltip.background.filters.clear();
  const seriesShadow = new am4core.DropShadowFilter();
  seriesShadow.dx = 0;
  seriesShadow.dy = 4;
  seriesShadow.blur = 8;
  seriesShadow.color = am4core.color("#000");
  seriesShadow.opacity = 0.4;
  seriesTooltip.background.filters.push(seriesShadow);

  chart.cursor = new am4charts.XYCursor();
  chart.cursor.lineX.stroke = am4core.color("#FFF");
  chart.cursor.lineX.strokeOpacity = 0.5;
  chart.cursor.lineX.strokeDasharray = "10";
  chart.cursor.lineY.stroke = am4core.color("#FFF");
  chart.cursor.lineY.strokeOpacity = 0.5;
  chart.cursor.lineY.strokeDasharray = "10";
  chart.cursor.behavior = "none";

  const axisTooltip = new am4core.Tooltip();
  dateAxis.tooltip = axisTooltip;
  axisTooltip.background.pointerLength = 6;
  axisTooltip.background.pointerBaseWidth = 10;
  axisTooltip.background.fill = am4core.color("#131313");
  axisTooltip.background.fillOpacity = 1;
  axisTooltip.background.strokeWidth = 0;
  axisTooltip.background.cornerRadius = 8;
  axisTooltip.label.paddingTop = 12;
  axisTooltip.label.paddingBottom = 12;
  axisTooltip.label.paddingRight = 16;
  axisTooltip.label.paddingLeft = 16;
  axisTooltip.label.fontSize = 12;
  axisTooltip.label.dy = 1;
  axisTooltip.background.dy = 1;

  axisTooltip.background.filters.clear();
  const axisShadow = new am4core.DropShadowFilter();
  axisShadow.dx = 0;
  axisShadow.dy = 4;
  axisShadow.blur = 8;
  axisShadow.color = am4core.color("#000");
  axisShadow.opacity = 0.4;
  axisTooltip.background.filters.push(axisShadow);

  // data ===================================

  const maxValue = Array.from(incomingData).sort((a, b) => b.value - a.value)[0]
    .value;
  const minValue = Array.from(incomingData).sort((a, b) => b.value - a.value)[
    incomingData.length - 1
  ].value;

  valueAxis.min = minValue / 5;
  valueAxis.max = maxValue * 1.15;

  const chartData = Array.from(incomingData);

  const firstDate = new Date(incomingData[0].date);
  firstDate.setDate(firstDate.getDate() - 1);

  const lastDate = new Date(incomingData[incomingData.length - 1].date);
  lastDate.setDate(lastDate.getDate() + 1);

  chartData.unshift({
    date: firstDate,
    value: incomingData[0].value,
  });

  chartData.push({
    date: lastDate,
    value: incomingData[incomingData.length - 1].value,
  });

  chart.data = chartData;
};

export default initChart;
